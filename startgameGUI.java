import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class startgameGUI extends JFrame{
    private Trainer trainer;

    public startgameGUI(Trainer trainer){
        super("Pokemon Game");
        this.trainer = trainer;
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();
        ImageIcon img = new ImageIcon("startgameBG.png");
        ImageIcon BTfirst = new ImageIcon("BTfirst.png");
        
        JLabel pic = new JLabel(img);
        c.add(pic);
        pack();

        JButton Play = new JButton("");
        Play.setBounds(450,500,400,100);
        Play.setIcon(BTfirst);
        // Play.setFont(new Font("Pokemon X and Y", Font.PLAIN, 60));    
        pic.add(Play);

        Play.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new starterGUI(trainer);
                setVisible(false);
            }
        });
    }
}