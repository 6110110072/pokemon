import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class starterGUI extends JFrame{
    private Trainer trainer;

    public starterGUI(Trainer trainer){
        super("Pokemon Game");
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();

        ImageIcon img = new ImageIcon("newSelectstarterBG.png");
        ImageIcon BTthorchic = new ImageIcon("BTthorchic.png");
        ImageIcon BTmudkip = new ImageIcon("BTmudkip.png");
        ImageIcon BTtreecko = new ImageIcon("BTtreecko.png");

        JLabel pic = new JLabel(img);
        c.add(pic);

        // JLabel starterlabel = new JLabel("Select your Pokemon");
        // starterlabel.setBounds(120,60,800,60);
        // starterlabel.setFont(new Font("Ketchum", Font.PLAIN, 60));
        // starterlabel.setForeground(Color.red);
        
        JButton firstPokemon = new JButton("");
        firstPokemon.setBounds(100,650,200,50);
        firstPokemon.setIcon(BTthorchic);
        // firstPokemon.setFont(new Font("Pokemon X and Y", Font.PLAIN, 30));

        JButton secondPokemon = new JButton("");
        secondPokemon.setBounds(550,650,200,50);
        secondPokemon.setIcon(BTmudkip);
        // secondPokemon.setFont(new Font("Pokemon X and Y", Font.PLAIN, 30));

        JButton thirdPokemon = new JButton("");
        thirdPokemon.setBounds(1050,650,200,50);
        thirdPokemon.setIcon(BTtreecko);
        // thirdPokemon.setFont(new Font("Pokemon X and Y", Font.PLAIN, 30));
        
        // pic.add(starterlabel);
        pic.add(firstPokemon);
        pic.add(secondPokemon);
        pic.add(thirdPokemon);
        // pic.add(starterlabel);
        
        Pokemon Torchic = new Torchic("Torchic of Satochi ");
        Pokemon Treecko = new Treecko("Treecko of Satochi ");
        Pokemon Mudkip = new Mudkip("Mudkip of Satochi ");

        firstPokemon.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                trainer.AddPokemon(Torchic);
                new SelectskillGUI(trainer);
                setVisible(false);
                
            }
        });

        secondPokemon.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                trainer.AddPokemon(Mudkip);
                new SelectskillGUI(trainer);
                setVisible(false);
            }
        });

        thirdPokemon.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                trainer.AddPokemon(Treecko);
                new SelectskillGUI(trainer);
                setVisible(false);
            }
        });
        pack();
        
    }
    
}