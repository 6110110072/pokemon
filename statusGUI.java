import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class statusGUI extends JFrame{
    private Trainer trainer;
    private int num;
    public statusGUI(Trainer trainer, int num){
        super("Pokemon Game");
        this.trainer = trainer;
        this.num = num;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        Container c = getContentPane();
        ImageIcon img = new ImageIcon("waterBG.png");
        ImageIcon fireBG = new ImageIcon("fireBG.png");
        ImageIcon waterBG = new ImageIcon("waterBG.png");
        ImageIcon grassBG = new ImageIcon("grassBG.png");
        ImageIcon eleBG = new ImageIcon("eleBG.png");
        ImageIcon poisonBG = new ImageIcon("poisonBG.png");
        ImageIcon BTBackPotion = new ImageIcon("BTBackPotion.png");
        JLabel pic = new JLabel(img);
        c.add(pic);
        pack();

        JButton Back = new JButton("");
        Back.setBounds(20,20,100,40);
        Back.setIcon(BTBackPotion);
        // Back.setFont(new Font("Pokemon X and Y", Font.PLAIN, 20));
        pic.add(Back);

        ImageIcon torchic = new ImageIcon("torchic1.png");
        ImageIcon Evee = new ImageIcon("Evee.png");
        ImageIcon Kabigon = new ImageIcon("Kabigon.png");
        ImageIcon Mudkip = new ImageIcon("Mudkip.png");
        ImageIcon Pikachu = new ImageIcon("Pikachu.png");
        ImageIcon Raichu = new ImageIcon("Raichu.png");
        ImageIcon Senikame = new ImageIcon("Senikame.png");
        ImageIcon Treecko = new ImageIcon("Treecko.png");

        JLabel image = new JLabel("");
        image.setBounds(90,0,500,800);
        pic.add(image);

        if((trainer.GetPokemon(num).getunit()) == 1){
            image.setIcon(torchic);
            pic.setIcon(fireBG);

        }
        else if((trainer.GetPokemon(num).getunit()) == 2){
            image.setIcon(Evee);
            pic.setIcon(poisonBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 3){
            image.setIcon(Kabigon);
            pic.setIcon(grassBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 4){
            image.setIcon(Mudkip);
            pic.setIcon(waterBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 5){
            image.setIcon(Pikachu);
            pic.setIcon(eleBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 6){
            image.setIcon(Raichu);
            pic.setIcon(eleBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 7){
            image.setIcon(Senikame);
            pic.setIcon(waterBG);
        }
        else if((trainer.GetPokemon(num).getunit()) == 8){
            image.setIcon(Treecko); 
            pic.setIcon(grassBG);
        }

        JLabel name = new JLabel(" NAME : " + trainer.GetPokemon(num).getName());
        JLabel hp_sp = new JLabel(" HP : " + trainer.GetPokemon(num).getHp() + " / " + trainer.GetPokemon(num).MaxHP()
        + " SP : " + trainer.GetPokemon(num).getSp()+ " / " + trainer.GetPokemon(num).MaxSP());
        JLabel type = new JLabel(" TYPE : " + trainer.GetPokemon(num).gettype());
        name.setFont(new Font("Ketchum", Font.PLAIN, 30));
        hp_sp.setFont(new Font("Ketchum", Font.PLAIN, 30));
        type.setFont(new Font("Ketchum", Font.PLAIN, 30));
        name.setBounds(800,40,800,100);
        hp_sp.setBounds(800,100,500,100);
        type.setBounds(800,160,500,100);
        pic.add(name);
        pic.add(hp_sp);
        pic.add(type);

        JLabel text = new JLabel("========== Skill ========== ");
        //text.setForeground(Color.red);
        JLabel skill1 = new JLabel(trainer.GetPokemon(num).getskillGUI(1));
        JLabel skill2 = new JLabel(trainer.GetPokemon(num).getskillGUI(2));
        JLabel skill3 = new JLabel(trainer.GetPokemon(num).getskillGUI(3));
        JLabel skill4 = new JLabel(trainer.GetPokemon(num).getskillGUI(4));
        text.setFont(new Font("Ketchum", Font.PLAIN, 30));
        skill1.setFont(new Font("Ketchum", Font.PLAIN, 25));
        skill2.setFont(new Font("Ketchum", Font.PLAIN, 25));
        skill3.setFont(new Font("Ketchum", Font.PLAIN, 25));
        skill4.setFont(new Font("Ketchum", Font.PLAIN, 25));
        text.setBounds(800,240,800,100);
        skill1.setBounds(800,320,800,100);
        skill2.setBounds(800,380,800,100);
        skill3.setBounds(800,440,800,100);
        skill4.setBounds(800,500,800,100);
        pic.add(text);
        pic.add(skill1);
        pic.add(skill2);
        pic.add(skill3);
        pic.add(skill4);

        Back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new bagGUI(trainer);
                setVisible(false);
            }
        });
    }
}