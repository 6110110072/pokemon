import javax.lang.model.util.ElementScanner6;

public class Torchic extends Pokemon{
    public Torchic(String name){
        super(name, 200, 250, "Fire", 1);
    }

    public void attack(Pokemon enemy,int dmg){
        System.out.println( name + " - Attack - " + enemy.getName());
        enemy.damage(dmg);

    }
    public void reduce(Pokemon mypokemon,int value){
        mypokemon.reduceSP(value);
        
    }

    public int AdditonalDmg(Pokemon enemy, int dmg){
        if(enemy.gettype().equals("Grass")){
            return dmg + 10;
        }
        else if(enemy.gettype().equals("Water")){
            return dmg - 10;
        }
        else
            return dmg;
    }
    
}